#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    MeshPtr _surface;
    int _current_N = 100;
    int _last_N;
    bool _draw_grid = false;
    bool _draw_debug_quad = false;

    MeshPtr _backgroundCube;
    MeshPtr _quad;

    glm::vec3 _mirror_center;
    float _mirror_width;
    float _mirror_height;
    glm::vec3 _mirror_angles;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _shader_grid;
    ShaderProgramPtr _skyboxShader;
    ShaderProgramPtr _mirrorShader;
    ShaderProgramPtr _quadShader;

    TexturePtr _cubeTex;
    GLuint _cubeTexSampler;

    float _refraction_index=0.1;

    GLuint _framebufferId;
    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;
    GLuint _renderTexId;
    GLuint _sampler;

    CameraInfo _fbCamera;


    SampleApplication() {
        _mirror_center = glm::vec3(-3, 0, 0);
        _mirror_width = 1;
        _mirror_height = 1;
        _mirror_angles = glm::vec3(0, 0, 0);
    }

    MeshPtr make_mirror()
    {

        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> tex_coords;

        auto rot_mat = glm::mat3_cast(glm::quat(_mirror_angles));

        vertices.push_back(glm::vec3(0, -_mirror_width, -_mirror_height));
        vertices.push_back(glm::vec3(0, -_mirror_width, _mirror_height));
        vertices.push_back(glm::vec3(0, _mirror_width, -_mirror_height));

        vertices.push_back(glm::vec3(0, _mirror_width, -_mirror_height));
        vertices.push_back(glm::vec3(0, -_mirror_width, _mirror_height));
        vertices.push_back(glm::vec3(0, _mirror_width, _mirror_height));

        for (int i = 0; i < 6; ++i) {
            vertices[i] = rot_mat * vertices[i] + _mirror_center;
        }
        for (int i = 0; i < 6; ++i) {
            glm::vec4 coord = _fbCamera.projMatrix * _fbCamera.viewMatrix * glm::vec4(vertices[i], 1);
            coord = ((coord / coord.w) + glm::vec4(1, 1, 1, 0)) / 2.0f;
            tex_coords.push_back(glm::vec2(coord.x, coord.y));
        }

        //----------------------------------------

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf1->setData(tex_coords.size() * sizeof(float) * 2, tex_coords.data());

        MeshPtr mesh = std::make_shared<Mesh>();
        mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        mesh->setAttribute(1, 2, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        mesh->setPrimitiveType(GL_TRIANGLES);
        mesh->setVertexCount(vertices.size());

        return mesh;
    }

    void initFramebuffer() {

        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);
        //----------------------------

        //Создаем текстуру, куда будет осуществляться рендеринг
        glGenTextures(1, &_renderTexId);
        glBindTexture(GL_TEXTURE_2D, _renderTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);

        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _renderTexId, 0);

        //----------------------------

        //Создаем буфер глубины для фреймбуфера
        GLuint depthRenderBuffer;
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, _fbWidth, _fbHeight);

        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthRenderBuffer);

        //----------------------------

        //Указываем куда именно мы будем рендерить
        GLenum buffers[] = { GL_COLOR_ATTACHMENT0 };
        glDrawBuffers(1, buffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void makeScene() override
    {
        Application::makeScene();

//        _surface = makeCube(1.0f);
        _surface = makeSurface(0.5, _current_N);
        _last_N = _current_N;
        _surface->setModelMatrix(glm::mat4(1.0f));
        _quad = makeScreenAlignedQuad();

        _backgroundCube = makeCube(10.0f);

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("699MakarovData3/shaderUBO.vert", "699MakarovData3/shaderUBO.frag");
        _shader_grid = std::make_shared<ShaderProgram>("699MakarovData3/black.vert", "699MakarovData3/black.frag");
        _skyboxShader = std::make_shared<ShaderProgram>("699MakarovData3/skybox.vert", "699MakarovData3/skybox.frag");
        _mirrorShader = std::make_shared<ShaderProgram>("699MakarovData3/mirror.vert", "699MakarovData3/mirror.frag");
        _quadShader = std::make_shared<ShaderProgram>("699MakarovData3/quadColor.vert", "699MakarovData3/quadColor.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

        _cubeTex = loadCubeTexture("699MakarovData3/cube");

        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        initFramebuffer();

        _fbCamera.viewMatrix = glm::lookAt(glm::vec3(5.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f));
        _fbCamera.projMatrix = glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 100.f);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_ESCAPE)
            {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
            if (key == GLFW_KEY_MINUS) {
                _current_N /= 1.5;
                if (_current_N < 10) {
                    _current_N = 10;
                }
            }
            if (key == GLFW_KEY_EQUAL) {
                _current_N *= 1.5;
                if (_current_N > 1000) {
                    _current_N = 1000;
                }
            }
            if (key == GLFW_KEY_Z) {
                _draw_grid ^= true;
            }
        }

        _cameraMover->handleKey(_window, key, scancode, action, mods);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Task", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::Checkbox("Draw grid", &_draw_grid);
            ImGui::SliderFloat("refraction index", &_refraction_index, 0.01f, 1.3f);

            ImGui::Checkbox("Draw debug quad", &_draw_debug_quad);
            ImGui::SliderFloat("Mirror width", &_mirror_width, 0, 10);
            ImGui::SliderFloat("Mirror height", &_mirror_height, 0, 10);

            ImGui::SliderFloat("Mirror x", &_mirror_center.x, -10, 10);
            ImGui::SliderFloat("Mirror y", &_mirror_center.y, -10, 10);
            ImGui::SliderFloat("Mirror z", &_mirror_center.z, -10, 10);

            ImGui::SliderFloat("Mirror angle x", &_mirror_angles.x, -glm::pi<float>() / 2, glm::pi<float>() / 2);
            ImGui::SliderFloat("Mirror angle y", &_mirror_angles.y, -glm::pi<float>() / 2, glm::pi<float>() / 2);
            ImGui::SliderFloat("Mirror angle z", &_mirror_angles.z, -glm::pi<float>() / 2, glm::pi<float>() / 2);
//            if (ImGui::CollapsingHeader("Light"))
//            {
//                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
//                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
//                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));
//
//                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
//                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
//                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
//            }
        }
        ImGui::End();
    }

    glm::vec3 mirror_over_plane(glm::vec3 point, glm::vec3 normal, glm::vec3 plane_point) {
        float pcoef = glm::dot(point, normal) - glm::dot(plane_point, normal);
        return point - 2 * pcoef * normal;
    }

    void update() override
    {
        Application::update();

        if (_current_N != _last_N) {
            _surface = makeSurface(0.5, _current_N);
            _last_N = _current_N;
            _surface->setModelMatrix(glm::mat4(1.0f));
        }

        auto camera_pos = dynamic_cast<OrbitCameraMover*>(_cameraMover.get())->get_camera_pos();
        auto mirror_normal = glm::mat3_cast(glm::quat(_mirror_angles)) * glm::vec3(1, 0, 0);
        _fbCamera.projMatrix = _camera.projMatrix;
        _fbCamera.viewMatrix = glm::lookAt(
            mirror_over_plane(camera_pos, mirror_normal, _mirror_center),
            mirror_over_plane(glm::vec3(0.0f, 0.0f, 0.5f), mirror_normal, _mirror_center),
            mirror_over_plane(glm::vec3(0.0f, 0.0f, 1.0f), mirror_normal, glm::vec3(0, 0, 0)));


    }

    void draw_scene(const CameraInfo& camera, bool draw_mirror)
    {
        _shader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClearColor(1, 1, 0, 1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glm::vec3 cameraPos = glm::vec3(glm::inverse(camera.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат
        //Для преобразования координат в текстурные координаты нужна специальная матрица
        glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        {
            _skyboxShader->use();


            _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
            _skyboxShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _skyboxShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

            _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _cubeTexSampler);
            _cubeTex->bind();
            _skyboxShader->setIntUniform("cubeTex", 0);

            glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

            _backgroundCube->draw();

            glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
        }

        //Загружаем на видеокарту значения юниформ-переменных

        glEnable(GL_POLYGON_OFFSET_LINE);
        //Подключаем шейдер
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glPolygonOffset(0, 0);
        _shader->use();

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
//        std::cout << cameraPos.x << ' ' << cameraPos.y << ' ' << cameraPos.z << std::endl;
        _shader->setVec3Uniform("camera_pos", cameraPos);
        _shader->setMat3Uniform("textureMatrix", textureMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _cubeTexSampler);
        _cubeTex->bind();
        _shader->setIntUniform("cubeTex", 0);

        _shader->setFloatUniform("refractionIndex", _refraction_index);

        _surface->draw();

        if (draw_mirror) {

            //Подключаем шейдер
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glPolygonOffset(0, 0);
            _mirrorShader->use();

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _mirrorShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _mirrorShader->setMat4Uniform("projectionMatrix", camera.projMatrix);
            _mirrorShader->setMat4Uniform("modelMatrix", _surface->modelMatrix());

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _sampler);
            _mirrorShader->setIntUniform("mirrorTex", 0);

            auto mirror = make_mirror();
            mirror->draw();


        }

        _shader_grid->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _shader_grid->setMat4Uniform("projectionMatrix", camera.projMatrix);

        if (_draw_debug_quad) {
            //Подключаем шейдер
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            _shader_grid->use();

            //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
            _shader_grid->setMat4Uniform("viewMatrix", camera.viewMatrix);
            _shader_grid->setMat4Uniform("projectionMatrix", camera.projMatrix);
            _shader_grid->setMat4Uniform("modelMatrix", _surface->modelMatrix());

            auto mirror = make_mirror();
            mirror->draw();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        if (_draw_grid) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glPolygonOffset(0, -10);
            _shader_grid->use();
            _shader_grid->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _surface->draw();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glPolygonOffset(0, 0);
        }
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);


        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);
        glViewport(0, 0, _fbWidth, _fbHeight);

        draw_scene(_fbCamera, false);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);

//        Отсоединяем фреймбуфер, чтобы теперь рендерить на экран
        glBindFramebuffer(GL_FRAMEBUFFER, 0);

        glViewport(0, 0, width, height);
        draw_scene(_camera, true);
        if (_draw_debug_quad) {
            _quadShader->use();

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindTexture(GL_TEXTURE_2D, _renderTexId);
            glBindSampler(0, _sampler);
            _quadShader->setIntUniform("tex", 0);

            glViewport(0, 0, 500, 500);

            _quad->draw();
        }
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}