#version 330

uniform sampler2D mirrorTex;

noperspective in vec2 texCoord; //текстурные координаты (интерполированы между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
	vec3 texColor = texture(mirrorTex, texCoord).rgb;

	fragColor = vec4(texColor, 1.0);
}
