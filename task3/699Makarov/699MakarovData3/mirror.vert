#version 330

uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

uniform mat3 modelMatrix; //матрица для превращения координат из локальной системы кординат в текстурные координаты

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec2 vertTexCoord;

noperspective out vec2 texCoord; //текстурные координаты

void main()
{
	texCoord = vertTexCoord;
	
	gl_Position = projectionMatrix * viewMatrix * vec4(vertexPosition, 1.0);
}
