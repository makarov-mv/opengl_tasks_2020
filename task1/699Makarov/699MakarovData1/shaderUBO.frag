/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330

in vec3 normal;

out vec3 fragColor;

void main()
{
    fragColor = normal * 0.5 + vec3(0.5);
}
