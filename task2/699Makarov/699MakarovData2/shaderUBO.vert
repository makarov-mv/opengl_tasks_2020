/*
Преобразует координаты вершины из локальной системы координат в Clip Space.
Копирует цвет вершины из вершинного атрибута в выходную переменную color.
*/

#version 330


layout(std140) uniform Matrices
{
    mat4 viewMatrix;
    mat4 projectionMatrix;
};

uniform mat4 modelMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

// uniform vec3 camera_pos;

out vec3 normal;
out vec3 global_pos;

void main()
{
    normal = mat3(transpose(inverse(modelMatrix))) * vertexNormal;
    global_pos = vec3(modelMatrix * vec4(vertexPosition, 1.0));
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
