/*
Получает на вход интеполированный цвет фрагмента и копирует его на выход.
*/

#version 330


uniform vec3 camera_pos;
uniform samplerCube cubeTex;
uniform mat3 textureMatrix;

uniform float refractionIndex;

in vec3 normal;
in vec3 global_pos;

out vec3 fragColor;

void main()
{
    vec3 camera_ray = normalize(global_pos - camera_pos);
    vec3 corr_normal = normalize(normal);
    if (dot(normal, camera_ray) > 0) {
        corr_normal = -corr_normal;
    }
    float fresnel = 0;
    if (abs(refractionIndex - 1) > 0.001) {
        float costh = -dot(corr_normal, camera_ray);
        //float sinth = sqrt(1 - costh * costh);

        costh = costh * refractionIndex;
        //sinth = sinth * refractionIndex;
        //sinth = sqrt(1 - sinth * sinth);
        //float fresnel = (costh - sinth) / (costh + sinth);
        //fresnel = fresnel * fresnel;

        float val = sqrt(1 - refractionIndex * refractionIndex + costh * costh) * costh;

        fresnel = 2 * costh * costh - 2 * val + 1 - refractionIndex * refractionIndex;
        fresnel /= refractionIndex * refractionIndex - 1;
        fresnel *= fresnel;
    }
    // fresnel = 0.0;
    vec3 reflected_pos = textureMatrix * reflect(camera_ray, corr_normal);
    vec3 refracted_pos = textureMatrix * refract(camera_ray, corr_normal, refractionIndex);

    fragColor = texture(cubeTex, reflected_pos).rgb * fresnel + texture(cubeTex, refracted_pos).rgb * (1 - fresnel);
}
