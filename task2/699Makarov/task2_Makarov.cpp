#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "Texture.hpp"

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/
class SampleApplication : public Application
{
public:
    MeshPtr _surface;
    int _current_N = 100;
    int _last_N;
    bool _draw_grid = false;

    MeshPtr _backgroundCube;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _shader_grid;
    ShaderProgramPtr _skyboxShader;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;

    TexturePtr _cubeTex;
    GLuint _cubeTexSampler;

    float _refraction_index=0.1;


    void makeScene() override
    {
        Application::makeScene();

//        _surface = makeCube(1.0f);
        _surface = makeSurface(0.5, _current_N);
        _last_N = _current_N;
        _surface->setModelMatrix(glm::mat4(1.0f));

        _backgroundCube = makeCube(10.0f);

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("699MakarovData2/shaderUBO.vert", "699MakarovData2/shaderUBO.frag");
        _shader_grid = std::make_shared<ShaderProgram>("699MakarovData2/shaderUBO.vert", "699MakarovData2/black.frag");
        _skyboxShader = std::make_shared<ShaderProgram>("699MakarovData2/skybox.vert", "699MakarovData2/skybox.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

        // Выведем размер Uniform block'а.
        GLint uniformBlockDataSize;
        glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
        std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
            glGenBuffers(1, &_ubo);
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
            glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
        // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);

        _cubeTex = loadCubeTexture("699MakarovData2/cube");

        glGenSamplers(1, &_cubeTexSampler);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glSamplerParameteri(_cubeTexSampler, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        if (action == GLFW_PRESS)
        {
            if (key == GLFW_KEY_ESCAPE)
            {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
            if (key == GLFW_KEY_MINUS) {
                _current_N /= 1.5;
                if (_current_N < 10) {
                    _current_N = 10;
                }
            }
            if (key == GLFW_KEY_EQUAL) {
                _current_N *= 1.5;
                if (_current_N > 1000) {
                    _current_N = 1000;
                }
            }
            if (key == GLFW_KEY_Z) {
                _draw_grid ^= true;
            }
        }

        _cameraMover->handleKey(_window, key, scancode, action, mods);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Task", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::Checkbox("Draw grid", &_draw_grid);
            ImGui::SliderFloat("refraction index", &_refraction_index, 0.01f, 1.3f);
//            if (ImGui::CollapsingHeader("Light"))
//            {
//                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
//                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
//                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));
//
//                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
//                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
//                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
//            }
        }
        ImGui::End();
    }

    void update() override
    {
        Application::update();

        if (_current_N != _last_N) {
            _surface = makeSurface(0.5, _current_N);
            _last_N = _current_N;
            _surface->setModelMatrix(glm::mat4(1.0f));
        }

        //Обновляем содержимое Uniform Buffer Object

        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
            {
                "viewMatrix",
                "projectionMatrix"
            };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

        // Вывод оффсетов.
        static bool hasOutputOffset = false;
        if (!hasOutputOffset) {
            std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
            hasOutputOffset = true;
        }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
        if (USE_DSA) {
            glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }
        else {
            glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
            glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
        }
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glm::vec3 cameraPos = glm::vec3(glm::inverse(_camera.viewMatrix)[3]); //Извлекаем из матрицы вида положение виртуальный камеры в мировой системе координат
//        std::cout << cameraPos.x << ' ' << cameraPos.y << ' ' << cameraPos.z << std::endl;
        //Для преобразования координат в текстурные координаты нужна специальная матрица
        glm::mat3 textureMatrix = glm::mat3(0.0f, 0.0f, -1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
        {
            _skyboxShader->use();


            _skyboxShader->setVec3Uniform("cameraPos", cameraPos);
            _skyboxShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
            _skyboxShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

            _skyboxShader->setMat3Uniform("textureMatrix", textureMatrix);

            glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
            glBindSampler(0, _cubeTexSampler);
            _cubeTex->bind();
            _skyboxShader->setIntUniform("cubeTex", 0);

            glDepthMask(GL_FALSE); //Отключаем запись в буфер глубины

            _backgroundCube->draw();

            glDepthMask(GL_TRUE); //Включаем обратно запись в буфер глубины
        }

        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);

        glEnable(GL_POLYGON_OFFSET_LINE);
        //Подключаем шейдер
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glPolygonOffset(0, 0);
        _shader->use();

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _surface->modelMatrix());
//        std::cout << cameraPos.x << ' ' << cameraPos.y << ' ' << cameraPos.z << std::endl;
        _shader->setVec3Uniform("camera_pos", cameraPos);
        _shader->setMat3Uniform("textureMatrix", textureMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _cubeTexSampler);
        _cubeTex->bind();
        _shader->setIntUniform("cubeTex", 0);

        _shader->setFloatUniform("refractionIndex", _refraction_index);

        _surface->draw();

        if (_draw_grid) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glPolygonOffset(0, -10);
            _shader_grid->use();
            _shader_grid->setMat4Uniform("modelMatrix", _surface->modelMatrix());
            _surface->draw();
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            glPolygonOffset(0, 0);
        }
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}